import React from 'react';
import {Link, Redirect, Route, Switch} from "react-router-dom";
import data from "../../exercise-2/mockups/data";
import "../styles/Content.css";


const Content = () => (
  <main className="content">
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route exact path="/products" component={Products}/>
      <Route path="/my-profile" component={Profile}/>
      <Route path="/about-us" component={About}/>
      <Redirect from="/goods" to="/products"/>
      <Route path="/products/:id" component={ProductDetail}/>
      <Redirect from="*" to="/"/>
    </Switch>
  </main>
);

const Home = () => (
  <div className="home">
    <p>This is beautiful Home Page.</p>
    <p>And the url is '/'.</p>
  </div>
);

const Products = () => (
  <div className="products">
    <h3>All Products:</h3>
    <ul>
      <li><Link to="/products/1">Bicycle</Link></li>
      <li><Link to="/products/2">TV</Link></li>
      <li><Link to="/products/3">Pencil</Link></li>
    </ul>


  </div>
);

const ProductDetail = ({match}) => {
  let productId = match.params.id;
  let productDetail = Object.values(data)[productId - 1];

  return (
    <div className="product-detail">
      <h3>Product Detail</h3>
      <p>Name: {productDetail.name}</p>
      <p>Id: {productDetail.id}</p>
      <p>Price: {productDetail.price}</p>
      <p>Desc: {productDetail.quantity}</p>
      <p>URL: {match.url}</p>
    </div>
  );
};

const Profile = () => (
  <div className="profile">
    <ul>
      <li>Username: XXX</li>
      <li>Gender: Female</li>
      <li>Work: IT Industry</li>
      <li>Website: '/my-profile'</li>
    </ul>
  </div>
);

const About = () => (
  <div className="about">
    <ul>
      <li>Company: ThoughtWorks Local</li>
      <li>Location: Xi'an</li>
    </ul>
    <br/>
    <p>For more information, please view our <Link to="/">website</Link>.</p>
  </div>
);

export default Content;