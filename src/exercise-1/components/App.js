import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router} from 'react-router-dom';
import Header from "./Header";
import Content from "./Content";

class App extends Component {
    render() {
        return (
            <div className="app">
                <Router>
                    <Header/>
                    <Content/>
                </Router>
            </div>
        );
    }
}

export default App;
