import React from 'react';
import NotMatch from "./NotMatch";

const User = ({match}) => {
  let userId = match.params.user;
  if (!isNaN(parseInt(userId))) {
    return (
      <div>
        User profile page.
      </div>
    );
  }
  return {NotMatch};
};

export default User;